pub const ELLIPSIS: &'static str = r#"
  <g>
    <circle cx="20" cy="50" r="10"/>
    <circle cx="50" cy="50" r="10"/>
    <circle cx="80" cy="50" r="10"/>
  </g>
"#;

pub const PLAY: &'static str = r#"
  <g><path d="m 19,10 c 10,0 70,30 70,40 0,10 -60,40 -70,40 -10,0 -10,-80 0,-80 z"/></g>
"#;

pub const PAUSE: &'static str = r#"
  <rect id="bar" width="20" height="70" x="20" y="15" rx="5" ry="5"/>
  <rect id="bar" width="20" height="70" x="60" y="15" rx="5" ry="5"/>
"#;
