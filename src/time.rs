use std::borrow::Borrow;

use stdweb::web::Date;

pub use time::Duration;

#[derive(Debug)]
pub struct SinceEpoch(Duration);

impl SinceEpoch {
    pub fn now() -> Self {
        let now_in_ms_since_epoch = Date::now();
        SinceEpoch(Duration::milliseconds(now_in_ms_since_epoch as i64))
    }

    /// In how long from now will it be this time
    pub fn how_long_until(&self) -> Duration {
        self.0 - Self::now().0
    }
}

impl<D: Borrow<Duration>> std::ops::Add<D> for SinceEpoch {
    type Output = SinceEpoch;

    fn add(self, other: D) -> SinceEpoch {
        SinceEpoch(self.0 + *other.borrow())
    }
}

pub fn is_maybe_daylightish() -> bool {
    match Date::new().get_hours() {
        9..=18 => true,
        _ => false,
    }
}
