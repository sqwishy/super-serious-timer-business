#[macro_use]
extern crate stdweb;
extern crate inhumane;

use std::cell::RefCell;

use yew::services::ConsoleService;
use yew::{html, Callback, Component, ComponentLink, Html, Renderable, ShouldRender};

mod duration_input;
mod icons;
mod svg;
mod time;
mod timer;

use crate::duration_input::DurationInput;
use crate::timer::Timer;

pub struct Model {
    console: RefCell<ConsoleService>,
    theme: String,
    show_input: bool,
    reset_timer: Option<Callback<time::Duration>>,
}

//#[derive(Debug)]
pub enum Msg {
    ToggleTheme,
    ToggleInput,
    RefTimerReset(Callback<time::Duration>),
    NewTimer(time::Duration),
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        let theme = if time::is_maybe_daylightish() {
            "theme-gb-light"
        } else {
            "theme-gb-dark"
        }
        .into();
        Model {
            console: RefCell::new(ConsoleService::new()),
            theme,
            show_input: false,
            reset_timer: None,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::ToggleTheme => {
                if self.theme == "theme-gb-dark" {
                    self.theme = "theme-gb-light".to_owned()
                } else {
                    self.theme = "theme-gb-dark".to_owned()
                };
                true
            }
            Msg::ToggleInput => {
                self.show_input = !self.show_input;
                if self.show_input {
                    // This is pobably a terrible thing to do but sending messages to child
                    // component is suffering ...
                    js! { @(no_return)
                        let input = document.getElementsByClassName("duration-input")[0];
                        if (input !== undefined) {
                            input.focus();
                            input.select();
                        }
                    }
                }
                true
            }
            // this shit is so fucking overcomplicated. we want to send a reset message to the
            // timer when the input dialog is accepted, so this message sets a callback that
            // sends that message to the timer. when we get Msg::NewTimer then we fire this
            // callback
            Msg::RefTimerReset(reset_cb) => {
                self.reset_timer = Some(reset_cb);
                false
            }
            Msg::NewTimer(duration) => {
                if let Some(reset_timer) = &self.reset_timer {
                    self.show_input = false;
                    reset_timer.emit(duration);
                    true
                } else {
                    false
                }
            }
        }
    }
}

impl Renderable<Model> for Model {
    fn view(&self) -> Html<Self> {
        self.console.borrow_mut().log("root is redrawing");

        html! {
          <div class=("meme", self.theme.clone()),>
            <button onclick=|_| Msg::ToggleTheme, class="toggle-theme",/>
            <Timer:
                reset=|timer| Msg::RefTimerReset(timer),
                on_options=|()| Msg::ToggleInput,
                />
            <DurationInput:
                class={
                    Some((if self.show_input {
                        "visible"
                    } else {
                        "hidden"
                    }).into())
                },
                on_accept=|d| Msg::NewTimer(d),
                on_cancel=|()| Msg::ToggleInput,
                />
          </div>
        }
    }
}
