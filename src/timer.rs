use std::borrow::Borrow;
use std::cell::RefCell;

use yew::services::timeout::TimeoutTask;
use yew::services::{ConsoleService, Task, TimeoutService};
use yew::{html, Callback, Component, ComponentLink, Html, Renderable, ShouldRender};

use crate::icons;
use crate::svg::Svg;
use crate::time::{Duration, SinceEpoch};

pub struct Timer {
    console: RefCell<ConsoleService>,
    timeout: TimeoutService,
    // ...
    fire_expired: Callback<()>,
    fire_refresh: Callback<()>,
    fire_reset: Callback<Duration>,
    // ...
    props: Props,
    state: State,
    initial_duration: Duration, // RENAME TODO XXX FIXME
    // silly hack to reset css animations
    toggle: bool,
}

struct Running {
    expire_at: SinceEpoch,
    expiry_task: TimeoutTask,
    refresh_task: TimeoutTask,
}

enum State {
    Running(Running), // because mutating enums is either stupid hard or I'm a moron?
    Paused { time_left: Duration },
    Expired,
}

impl State {
    fn time_left(&self) -> Duration {
        match self {
            State::Running(Running { expire_at, .. }) => expire_at.how_long_until(),
            State::Paused { time_left } => *time_left,
            State::Expired => Duration::seconds(0),
        }
    }
}

#[derive(Debug)]
pub enum Msg {
    Toggle,
    Reset(Duration),
    Expired,
    Refresh,
    Options,
}

#[derive(PartialEq, Clone)]
pub struct Props {
    /// This is a callback for setting up a callback that will fire the Reset message on this
    /// component, allowing the timer duration to change and restart.
    pub reset: Option<Callback<Callback<Duration>>>,
    pub on_options: Option<Callback<()>>,
}

impl Default for Props {
    fn default() -> Self {
        Props {
            reset: None,
            on_options: None,
        }
    }
}

impl Component for Timer {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, mut link: ComponentLink<Self>) -> Self {
        let timer = Timer {
            console: RefCell::new(ConsoleService::new()),
            timeout: TimeoutService::new(),
            fire_expired: link.send_back(|_| Msg::Expired),
            fire_refresh: link.send_back(|_| Msg::Refresh),
            fire_reset: link.send_back(|d| Msg::Reset(d)),
            initial_duration: Duration::seconds(60),
            state: State::Paused {
                time_left: Duration::seconds(60),
            },
            toggle: false,
            props: props,
        };
        if let Some(reset) = &timer.props.reset {
            reset.emit(timer.fire_reset.clone());
        }
        timer
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Toggle => {
                match &mut self.state {
                    State::Running(running) => {
                        let Running {
                            expire_at,
                            expiry_task,
                            refresh_task,
                        } = running;
                        // stop
                        expiry_task.cancel();
                        refresh_task.cancel();
                        self.state = State::Paused {
                            time_left: expire_at.how_long_until(),
                        }
                    }
                    State::Paused { time_left } => {
                        let expire_at = SinceEpoch::now() + *time_left;
                        let time_left = time_left.to_std().unwrap();
                        let expiry_task = self.timeout.spawn(time_left, self.fire_expired.clone());
                        let refresh_task = self.timeout.spawn(
                            std::time::Duration::from_millis(time_left.subsec_millis() as u64),
                            self.fire_refresh.clone(),
                        );
                        self.state = State::Running(Running {
                            expire_at,
                            expiry_task,
                            refresh_task,
                        });
                    }
                    State::Expired => {
                        self.update(Msg::Reset(self.initial_duration));
                    }
                }
            }
            Msg::Refresh => {
                match &mut self.state {
                    State::Running(running) => {
                        if running.refresh_task.is_active() {
                            running.refresh_task.cancel();
                        }
                        if let Ok(time_left) = running.expire_at.how_long_until().to_std() {
                            let refresh_in =
                                std::time::Duration::from_millis(time_left.subsec_millis().into());
                            running.refresh_task =
                                self.timeout.spawn(refresh_in, self.fire_refresh.clone());
                        }
                    }
                    _ => (), /* TODO don't return true for ShouldRender? */
                }
            }
            Msg::Reset(new_duration) => match &mut self.state {
                State::Expired | State::Running { .. } | State::Paused { .. } => {
                    self.toggle = !self.toggle;
                    self.initial_duration = new_duration;
                    self.state = State::Paused {
                        time_left: self.initial_duration,
                    };
                    self.update(Msg::Toggle);
                }
            },
            Msg::Expired => {
                self.state = State::Expired;
            }
            Msg::Options => {
                if let Some(on_options) = &self.props.on_options {
                    on_options.emit(());
                }
            }
        }
        true
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props = props;
        if let Some(reset) = &self.props.reset {
            reset.emit(self.fire_reset.clone());
        }
        false
    }
}

impl Renderable<Timer> for Timer {
    fn view(&self) -> Html<Self> {
        self.console.borrow_mut().log("timer is redrawing");
        let duration = format!(
            "animation-duration: {}ms",
            self.initial_duration.num_milliseconds()
        );
        let state = match self.state {
            State::Running { .. } => "running",
            State::Paused { .. } => "paused",
            State::Expired => "expired",
        };
        let anim_hack = if self.toggle { "anim-hack" } else { "" };
        html! {
            <div class=("timer", state, anim_hack),>
              <div class="progress", style={duration},>
                <div class="fill-left",></div>
                <div class="fill-right",></div>
              </div>
              {timer_text(&self.state.time_left())}
              {toggle_button()}
              {options_button()}
            </div>
        }
    }
}

fn timer_text<D: Borrow<Duration>>(d: D) -> Html<Timer> {
    let secs = d.borrow().num_seconds();
    let (mins, secs) = divmod(secs, 60);
    let (hours, mins) = divmod(mins, 60);
    html! {
      <div class="text",>
        <span class="hr",>{ hours }</span>
        <span class="sep hr-min-sep",>{":"}</span>
        <span class="min",>{ format!("{:02}", mins) }</span>
        <span class="sep min-sec-sep",>{":"}</span>
        <span class="sec",>{ format!("{:02}",secs) }</span>
      </div>
    }
}

pub fn divmod<N: std::clone::Clone + std::ops::Div + std::ops::Rem>(
    n: N,
    m: N,
) -> (<N as std::ops::Div>::Output, <N as std::ops::Rem>::Output) {
    (n.clone() / m.clone(), n % m)
}

fn toggle_button() -> Html<Timer> {
    html! {
      <button tabIndex=1, onclick=|_| Msg::Toggle, class="toggle",>
        <Svg: class="play-icon", content=icons::PLAY,/>
        <Svg: class="pause-icon", content=icons::PAUSE,/>
      </button>
    }
}

fn options_button() -> Html<Timer> {
    html! {
      <button tabIndex=2, onclick=|_| Msg::Options, class="options",>
        <Svg: class="options-icon", content=icons::ELLIPSIS,/>
      </button>
    }
}
