/// https://github.com/DenisKolodin/yew/issues/328
use yew::{html, Component, ComponentLink, Html, Renderable, ShouldRender};

pub struct Svg {
    class: String,
    content: String,
}

#[derive(PartialEq, Clone)]
pub struct Props {
    pub class: String,
    pub content: String,
}

impl Default for Props {
    fn default() -> Self {
        Props {
            class: String::default(),
            content: String::default(),
        }
    }
}

impl Component for Svg {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        Svg {
            class: props.class,
            content: props.content,
        }
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.class = props.class;
        self.content = props.content;
        true
    }
}

impl Renderable<Svg> for Svg {
    fn view(&self) -> Html<Self> {
        svg_html(&self.class, &self.content)
    }
}

pub fn svg_html<M: Component>(class: &str, content: &str) -> Html<M> {
    use stdweb::unstable::TryFrom;
    use stdweb::web::{Element, IElement, Node};
    use yew::virtual_dom::VNode;

    let elem = Element::try_from(js! {
        return document.createElementNS("http://www.w3.org/2000/svg", "svg");
    })
    .unwrap();
    if !class.is_empty() {
        elem.set_attribute("class", class).unwrap();
    }
    elem.set_attribute("viewBox", "0 0 100 100").unwrap();
    elem.prepend_html(content).unwrap();
    html! { { VNode::VRef(Node::from(elem)) } }
}
