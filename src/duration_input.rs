use std::cell::RefCell;

use stdweb::traits::IEvent;
use yew::services::ConsoleService;
use yew::{html, Callback, Component, ComponentLink, Html, Renderable, ShouldRender};

pub use time::Duration;

use crate::icons;
use crate::svg::Svg;

pub struct DurationInput {
    console: RefCell<ConsoleService>,
    // ...
    input: String,
    duration_for_input: Option<Duration>,
    props: Props,
}

#[derive(PartialEq, Clone)]
pub struct Props {
    // TODO is there a better type than string? we can only use things that implement PartialEq so
    // this is a bit awkward ...
    pub class: Option<String>,
    pub on_accept: Option<Callback<Duration>>,
    pub on_cancel: Option<Callback<()>>,
}

impl Default for Props {
    fn default() -> Self {
        Props {
            class: None,
            on_accept: None,
            on_cancel: None,
        }
    }
}

#[derive(Debug)]
pub enum Msg {
    Input(String),
    Accept,
    Cancel,
}

impl Component for DurationInput {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        DurationInput {
            console: RefCell::new(ConsoleService::new()),
            input: "sixty seconds".to_owned(),
            duration_for_input: Some(time::Duration::seconds(60)),
            props: props,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Input(input) => {
                self.input = input;
                self.duration_for_input = inhumane::dehumanize(&self.input);
            }
            Msg::Accept => {
                if let Some(duration) = self.duration_for_input {
                    if let Some(on_accept) = &self.props.on_accept {
                        on_accept.emit(duration)
                    }
                }
            }
            Msg::Cancel => {
                if let Some(on_cancel) = &self.props.on_cancel {
                    on_cancel.emit(())
                }
            }
        }
        true
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.props = props;
        true
    }
}

impl Renderable<DurationInput> for DurationInput {
    fn view(&self) -> Html<Self> {
        self.console.borrow_mut().log("duration input is redrawing");
        let is_valid = self.duration_for_input.is_some();
        html! {
            <div class=(
                "duration-dialog",
                "visible",
                if is_valid { "valid" } else { "invalid" },
                self.props.class.as_ref().unwrap_or(&"".into())
            ),>
              <form onsubmit=|e| { e.prevent_default(); Msg::Accept },>
                <input
                    class="duration-input",
                    tabIndex=4,
                    type="text",
                    value={self.input.clone()},
                    oninput=|i| Msg::Input(i.value),
                    />
              </form>
              {self.accept_button()}
              {self.cancel_button()}
            </div>
        }
    }
}

impl DurationInput {
    fn accept_button(&self) -> Html<Self> {
        let is_valid = self.duration_for_input.is_some();
        html! {
          <button tabIndex=3, disabled={!is_valid}, onclick=|_| Msg::Accept, class="accept",>
            <Svg: class="accept-icon", content=include_str!("alarm-icon"),/>
          </button>
        }
    }

    fn cancel_button(&self) -> Html<Self> {
        html! {
          <button tabIndex=5, onclick=|_| Msg::Cancel, class="cancel",>
            <Svg: class="cancel-icon", content=icons::ELLIPSIS,/>
          </button>
        }
    }
}
