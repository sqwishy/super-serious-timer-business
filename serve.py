#!/usr/bin/env python3
"""
"borrowed" from Lib/http/server.py
"""

import os
from functools import partial
from http.server import SimpleHTTPRequestHandler, test

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--bind",
        "-b",
        default="",
        metavar="ADDRESS",
        help="Specify alternate bind address " "[default: all interfaces]",
    )
    parser.add_argument(
        "--directory",
        "-d",
        default=os.getcwd(),
        help="Specify alternative directory " "[default:current directory]",
    )
    parser.add_argument(
        "port",
        action="store",
        default=8000,
        type=int,
        nargs="?",
        help="Specify alternate port [default: 8000]",
    )
    args = parser.parse_args()

    class MemeHandler(SimpleHTTPRequestHandler):
        extensions_map = SimpleHTTPRequestHandler.extensions_map.copy()
        extensions_map.update({".wasm": "application/wasm"})

    handler_class = partial(MemeHandler, directory=args.directory)
    test(HandlerClass=handler_class, port=args.port, bind=args.bind)
