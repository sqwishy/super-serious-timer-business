use time::Duration;

pub fn unit_amounts(quantity: i64, unit: &str) -> Option<Duration> {
    match unit {
        "w" | "wk" | "wks" | "week" | "weeks" => Some(Duration::weeks(quantity)),
        "d" | "day" | "days" => Some(Duration::days(quantity)),
        "h" | "hr" | "hrs" | "hour" | "hours" => Some(Duration::hours(quantity)),
        "m" | "min" | "mins" | "minute" | "minutes" => Some(Duration::minutes(quantity)),
        "s" | "sec" | "secs" | "second" | "seconds" => Some(Duration::seconds(quantity)),
        "ms" | "millisecond" | "milliseconds" => Some(Duration::milliseconds(quantity)),
        "microsecond" | "microseconds" => Some(Duration::microseconds(quantity)),
        "nanosecond" | "nanoseconds" => Some(Duration::nanoseconds(quantity)),
        _ => None,
    }
}
