extern crate time;
#[macro_use]
extern crate lazy_static;
extern crate regex;

mod duration;

// seconds is a measurement of time
// meter is a measurement of length
// dozen is a measurement of ... ?
//
// It's also possible to combine measurements, so "N metre / second" measures "length / time" and
// implicitly means N metres per 1 second...
// Similarly, N metre meters or N m^2 is useful to describe the size of a plane or something.
//
// We can say "kilometer per per hour" which is like "1000 meters per 60 minutes", so we have a
// "per" operator over two measurements. And that itself can be used as the unit to some other
// measurement: "5 kilometers per hour" or "5 1000 meters per 60 minutes" mean the same thing.
//
// The problem is this: if every measurement is a unit, we shouldn't interpret "two dozen bananas"
// the same as "twenty four bananas"; the former aggregates as 2 * 12 and the latter as 20 + 4.
//
// It's like an order of operations thing?

/// todo divide these into categories? like non-zero, things that fit into u8, u16, ...?
fn quantity_words(s: &str) -> Option<u32> {
    match s {
        "zero" => Some(0),
        "a" | "an" | "one" => Some(1),
        "two" => Some(2),
        "three" => Some(3),
        "four" => Some(4),
        "five" => Some(5),
        "six" => Some(6),
        "seven" => Some(7),
        "eight" => Some(8),
        "nine" => Some(9),
        "ten" => Some(10),
        "eleven" => Some(11),
        "twelve" => Some(12),
        //"dozen" => Some(12),
        "thirteen" => Some(13),
        "fourteen" => Some(14),
        "fifteen" => Some(15),
        "sixteen" => Some(16),
        "seventeen" => Some(17),
        "eighteen" => Some(18),
        "nineteen" => Some(19),
        "twenty" => Some(20),
        "thirty" => Some(30),
        "forty" => Some(40),
        "fifty" => Some(50),
        "sixty" => Some(60),
        "seventy" => Some(70),
        "eighty" => Some(80),
        "ninety" => Some(90),
        //"hundred" => Some(100),
        //"thousand" => Some(1_000),
        //"million" => Some(1_000_000),
        //"billion" => Some(1_000_000_000),
        _ => None,
    }
}

#[derive(Debug)]
enum Term<'a> {
    Word(&'a str),
    Number(i64),
}

pub fn dehumanize(text: &str) -> Option<time::Duration> {
    use regex::Regex;
    lazy_static! {
        static ref RE: Regex = Regex::new(r"([A-Za-z]+)|([0-9]+)").unwrap();
    }

    let terms = RE.captures_iter(text).filter_map(|capture| {
        if let Some(match_) = capture.get(1) {
            let word = &text[match_.start()..match_.end()];
            Some(Term::Word(word))
        } else if let Some(match_) = capture.get(2) {
            text[match_.start()..match_.end()]
                .parse()
                .ok()
                .map(Term::Number)
        } else {
            None
        }
    });

    let mut scalar = None;
    let mut aggregate: Option<time::Duration> = None;

    for term in terms {
        match term {
            Term::Number(n) => scalar = Some(scalar.unwrap_or(0) + n),
            Term::Word(word) => {
                if let Some(duration) = duration::unit_amounts(scalar.unwrap_or(1), word) {
                    aggregate = if let Some(current) = aggregate {
                        current + duration
                    } else {
                        duration
                    }
                    .into();
                    scalar = None
                } else if let Some(quantity) = quantity_words(word) {
                    scalar = Some(scalar.unwrap_or(0) + quantity as i64)
                }
            }
        }
    }
    aggregate
}

#[cfg(test)]
mod tests {
    use super::*;
    use time::Duration;

    #[test]
    fn test_text_to_duration() {
        assert_eq!(dehumanize("one minute"), Some(Duration::minutes(1)));
        assert_eq!(dehumanize("90 seconds"), Some(Duration::seconds(90)));
        assert_eq!(dehumanize("two hours and a day"), Some(Duration::hours(26)));
        assert_eq!(dehumanize("1h30m"), Some(Duration::minutes(90)));
        assert_eq!(dehumanize("1:30"), None); /* :( */
    }

    #[test]
    fn test_words() {
        //assert_eq!(dehumanize("two dozen minutes"), Some(Duration::minutes(24)));
        //assert_eq!(dehumanize("two hundred min"), Some(Duration::minutes(200)));
        assert_eq!(dehumanize("sixty-six minutes"), Some(Duration::minutes(66)));
    }
}
