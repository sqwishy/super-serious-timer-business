Super Serious Timer Business
----------------------------

Try it out at https://timer.froghat.ca.

Small project to try out rust & WebAssembly. This uses a rust library called `yew
<https://github.com/DenisKolodin/yew>`_.
